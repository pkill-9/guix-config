#!/bin/sh

# Install FHS support for Guix System

FHS_CODE_DIR=/home/itsme/.config/guix
FHS_CODE_FILENAME=fhs-code.scm
TMP=/tmp

write_build_script () {
# Build either "glibc-for-fhs", "glibc-for-fhs-x86" or "ld.so.cache" using fhs-code.scm by simply copying fhs-code.scm and appending the desired variable to build, which is the first argument to this function
cp $FHS_CODE_DIR/$FHS_CODE_FILENAME $TMP
echo $1 >> $TMP/$FHS_CODE_FILENAME
}

write_build_script glibc-for-fhs
mkdir -p /lib64
cd /lib64
guix build -r glibc-for-fhs -f $TMP/fhs-code.scm
ln -sf glibc-for-fhs-1/lib/ld-linux-x86-64.so.2

write_build_script glibc-for-fhs-x86
mkdir -p /lib
cd /lib
guix build -r glibc-for-fhs-x86 -f $TMP/fhs-code.scm
ln -sf glibc-for-fhs-x86-1/lib/ld-linux-x86-64.so.2

write_build_script ld.so.cache
cd /etc
guix build -r ld.so.cache -f $TMP/fhs-code.scm

;; TODO: sandboxed package nto installign when appended to list of packages
;; TODO: https://guix.gnu.org/manual/devel/en/html_node/Essential-Home-Services.html
(use-modules (gnu home)
             (gnu home services)
             (gnu home services shells) ;; Shells
             (gnu home services guix) ;; Channels
	     (guix channels) ;; contains (channel)
             (gnu services)
             (gnu packages admin)
	     (gnu packages)
             (guix gexp))

;; =============================
;; === Non-Sandboxed Packages ==
;; =============================

(define %packages (specifications->packages (list

;; ==== Desktop ==
;; ===============

"gnome-keyring"
"mpv"
"flatpak"
"zathura-pdf-poppler"

;; === Emacs =====
;; ===============

"emacs-paredit"
"emacs-geiser"
"emacs-yasnippet"
"emacs"
"emacs-guix"
"emacs-magit"
"emacs-evil"
"emacs-rainbow-delimiters"
"emacs-origami-el"
"emacs-lua-mode"

;; ==== Debug ====
;; ===============

"strace"
"gdb"
"nss-certs"
"glibc"

;; == Filesystem =
;; ===============

"sshfs"
"cryptsetup"
"gvfs"
"bindfs"
"unionfs-fuse"
"dosfstools"

;; === Fonts =====
;; ===============

"font-gnu-freefont"
"font-ghostscript"


;; === Hardware ==
;; ===============

"dmidecode"
"acpi"
"lshw"
"powertop"
"smartmontools"
"tlp"
"lm-sensors"
"udiskie"

;; === Network ===
;; ===============

"wireguard-tools"
"netcat"
"curl"
"wget"

;; == Programming ==
;; =================

"guile"
"python2"

;; === Shell =======
;; =================

"atool"
"file"
"tree"
"st"
"xterm"
"fish"
"lf"
"ripgrep"
"wterm"
"direnv"
"fd"
"tmux"
"moreutils"
"git"
"git-lfs"
"pv"
"alacritty"
"socat"
"ncurses"
"fzf"
"foot"

;; === System ======
;; =================

"binutils"
"pulseaudio"
"pavucontrol"
"htop"
"iotop"
"neovim"
"pinentry"
"jack"
"jack2"
"qjackctl"
"ghc-pandoc"
"lsof"
"imagemagick"
"password-store"
"links"
"folks"
"bubblewrap"
"gnupg"
"flatpak"
"zip"
"unzip"
"bashtop"
"glibc-locales"

;; === Themes ======
;; =================

"breeze-icons"
"adwaita-icon-theme"
"hicolor-icon-theme" ;; Geary icons break without this

;; === WM Utils=====
;; =================

"xbacklight"
"xev"
"unclutter"
"xclip"
"xmodmap"
"xinput"
"xrandr"
"setxkbmap"
"xhost"
"xdotool"
"xdg-utils"
"rofi"
"brightnessctl"
"slurp"
"grim"
"wl-clipboard"
"waybar"
"xset"
"mako"
"libnotify"
"swayidle"
"sway"
"swaybg"
)))

;; ===============
;; ===============
;; ===============

;; ===============
;; === Main ======
;; ===============

(home-environment
 (packages %packages)
 (services (list
;;    (service home-bash-service-type
;;            (home-bash-configuration
;;             (guix-defaults? #t)
;;             (bash-profile (list (local-file (string-append (getenv "HOME") "/.config/guix/profile-dotfile"))))))
   (simple-service 'variant-packages-service
                home-channels-service-type
                (list
                 (channel
                  (name 'nonguix)
                  (url "https://gitlab.com/nonguix/nonguix.git")))
 ))))

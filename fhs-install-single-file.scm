#!/usr/bin/env -S guile --no-auto-compile
!#

;;TODO: Check whether existing symlink exists and give error if it's not correctly pointed
;;TODO: Use guix environment so it doesn't fail on existing GC root
;;TODO: Add special symlinks like fontconfig
;;==================================
;;==== This is the FHS code ========
;;==================================
(define %fhs-code
"
(use-modules
	       (gnu packages)
	       (gnu packages base)
	       (srfi srfi-1)
	       (guix gexp)
	       (guix build-system trivial)
	       (guix profiles)
	       (guix packages))

;;===== Glibc =====

(define (32bit-package pkg)
  (package (inherit pkg)
           (name (string-append (package-name pkg) \"-i686-linux\"))
           (arguments
            `(#:system \"i686-linux\"
              ,@(package-arguments pkg)))))

(define glibc-for-fhs
  (package (inherit glibc)
           (name \"glibc-for-fhs\") ;; Maybe rename this to \"glibc-wi th-ldconfig-for-fhs\"
           (source (origin
                    (inherit (package-source glibc))
                    (patches (delete (car (search-patches \"glibc-dl-cache.patch\"))
                                     (origin-patches (package-source glibc)))))))) ;; Re-enable ldconfig

(define glibc-for-fhs-x86
    (32bit-package glibc-for-fhs))

;; =================

(define (packages->ld.so.conf packages)
  (computed-file
   \"ld.so.conf\"
   (with-imported-modules
    `((guix build union)
      (guix build utils))
    #~(begin
        (use-modules (guix build union)
                     (guix build utils))
        (let* ((packages '#$packages) ;; Need to quote \"#$packages\" as #$packages tries to \"apply\" the first item to the rest, like a procedure.
               (find-lib-directories-in-single-package
                (lambda (package)
                  (find-files (string-append package \"/lib\")
                              (lambda (file stat)
                                ;; setting keyword \"stat\" to \"stat\" means it will follow
                                ;; symlinks, unlike what it's set to by default (\"lstat\").
                                (eq? 'directory (stat:type stat)))
                              #:stat stat
                              #:directories? #t)))
               (find-lib-directories-in-all-packages
                (lambda (packages)
                  (apply append ;; Concatenate the directory lists from \"map\" into one list
                         (map (lambda (package)
                                (find-lib-directories-in-single-package package))
                              packages))))
               (fhs-lib-dirs
                 (find-lib-directories-in-all-packages packages)))
               (with-output-to-file
                   #$output
                 (lambda _
                   (format #t
                           (string-join fhs-lib-dirs \"\n\"))
                   #$output)))))))

(define (ld.so.conf->ld.so.cache ld-conf)
  (computed-file \"ld.so.cache\"
                 (with-imported-modules `((guix build utils))
                                        #~(begin
                                            (use-modules (guix build utils))
                                            (let* ((ldconfig (string-append #$glibc-for-fhs \"/sbin/ldconfig\")))
                                              (invoke ldconfig
                                                      \"-X\" ;; Don't update symbolic links
                                                      \"-f\" #$ld-conf ;; Use #$configuration as configuration file
                                                      \"-C\" #$output)))))) ;; Use #$output as cache file

(define (packages->ld.so.cache packages)
  (ld.so.conf->ld.so.cache (packages->ld.so.conf packages)))

(define (manifest->packages manifest) ;; copied from guix/scripts/refresh.scm, referring to it with (@@ ...) stopped working for some reason, kept saying it's an unbound variable
  \"Return the list of packages in MANIFEST.\"
  (filter-map (lambda (entry)
                (let ((item (manifest-entry-item entry)))
                  (if (package? item) item #f)))
              (manifest-entries manifest)))

(define (package-output->package original-package package-output)
  (package
   (name (string-append (package-name original-package) \"-\" package-output))
   (version (package-version original-package))
   (source #f)
   (build-system trivial-build-system)
   (inputs
    `((\"package\" ,original-package ,package-output)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs \"out\"))
               (original-package (assoc-ref %build-inputs \"package\")))
          (symlink original-package out)))))
   (home-page #f)
   (synopsis (string-append \"Output \" package-output
                            \" of package: \" (package-name original-package)))
   (description synopsis)
   (license (package-license original-package))))

(define* (union name packages #:key options)
  (computed-file name
                 (with-imported-modules `((guix build union))
                                        #~(begin
                                            (use-modules (guix build union))
                                            (union-build #$output '#$packages)))
                 #:options options))

(define fhs-packages ;; list of packages to use for fhs-service
  (append (list (package-output->package (@ (gnu packages gcc) gcc-7) \"lib\"))
          (manifest->packages
           (specifications->manifest
            (list
	      \"glibc\"
             \"libxcomposite\" \"libxtst\" \"libxaw\" \"libxt\" \"libxrandr\" \"libxext\" \"libx11\"
             \"libxfixes\" \"glib\" \"gtk+\" \"gtk+@2\" \"bzip2\" \"zlib\" \"gdk-pixbuf\" \"libxinerama\"
             \"libxdamage\" \"libxcursor\" \"libxrender\" \"libxscrnsaver\" \"libxxf86vm\"
             \"libxi\" \"libsm\" \"libice\" \"gconf\" \"freetype\" \"curl\" \"nspr\" \"nss\" \"fontconfig\"
             \"cairo\" \"pango\" \"expat\" \"dbus\" \"cups\" \"libcap\" \"sdl2\" \"libusb\" \"dbus-glib\"
             \"atk\" \"eudev\" \"network-manager\" \"pulseaudio\" \"openal\" \"alsa-lib\" \"mesa\"
             \"libxmu\" \"libxcb\" \"glu\" \"util-linux\" \"util-linux:lib\" \"libogg\" \"libvorbis\" \"sdl\" \"sdl2-image\"
             \"glew\" \"openssl\" \"openssl@1\" \"libidn\" \"tbb\" \"flac\" \"freeglut\" \"libjpeg\" \"libpng\"
             \"libsamplerate\" \"libmikmod\" \"libtheora\" \"libtiff\" \"pixman\" \"speex\" \"sdl-image\"
             \"sdl-ttf\" \"sdl-mixer\" \"sdl2-ttf\" \"sdl2-mixer\" \"gstreamer\" \"gst-plugins-base\"
             \"glu\" \"libcaca\" \"libcanberra\" \"libgcrypt\" \"libvpx\"
             ;;\"librsvg\" ;; currently requires compiling, but shouldn't, it's being weird
             \"libxft\"
             \"libvdpau\" \"gst-plugins-ugly\" \"libdrm\" \"xkeyboard-config\" \"libpciaccess\"
             ;;\"ffmpeg@3.4\" ;; Disable this because test fails at livf-something (373grdi4fc369v2h29g10672whmv0mvb-ffmpeg-3.4.7.drv)
             \"libpng\" \"libgpg-error\" \"sqlite\" \"libnotify\"
	     \"qtbase@5\"

             \"fuse\" \"e2fsprogs\" \"p11-kit\" \"xz\" \"keyutils\" \"xcb-util-keysyms\" \"libselinux\"
             \"ncurses\" \"jack\" \"jack2\" \"vulkan-loader\" \"at-spi2-atk\" \"at-spi2-core\" \"libsigc++\"
	     \"mit-krb5\" \"libxml2\" \"libsndfile\" \"glew\" \"libsecret\" \"harfbuzz\")))))

(define* (fhs-libs-union packages #:key system)
  (let* ((name (if system
                   (string-append \"fhs-libs-\" system)
                   \"fhs-libs\")))
    (union name
           packages
           #:options `(#:system ,system))))

(define ld.so.cache
(let ((fhs-packages-both-archs (append fhs-packages
				 `(,(fhs-libs-union fhs-packages #:system \"i686-linux\")))))

	(packages->ld.so.cache fhs-packages-both-archs)))

;;glibc-for-fhs
;;glibc-for-fhs-x86
;;ld.so.cache
")

;;========================================================================
;;========================================================================
;;========================================================================

;;============
;;=== Main ===
;;============

(define (write-fhs-code-scm var)
    ;; Write the main part with all the definitions. var is the package variable for %fhs-code to build when calling `guix build -f`
    (let ((output-port (open-file "/tmp/fhs-code.scm" "w")))
      (display %fhs-code output-port)
      (newline output-port)
      (close output-port))
    
    ;; Append the variable
    (let ((output-port (open-file "/tmp/fhs-code.scm" "a")))
      (display var output-port)
      (newline output-port)
      (close output-port))
)

;;glibc x86-64 (/lib64)
(write-fhs-code-scm "glibc-for-fhs")
(system "mkdir -p /lib64")
(chdir "/lib64")
(system* "guix" "build" "-r" "glibc-for-fhs" "-f" "/tmp/fhs-code.scm")
(symlink "glibc-for-fhs-1/lib/ld-linux-x86-64.so.2" "ld-linux-x86-64.so.2")

;;glibc x86 (/lib)
(write-fhs-code-scm "glibc-for-fhs-x86")
(system "mkdir -p /lib")
(chdir "/lib")
(system* "guix" "build" "-r" "glibc-for-fhs" "-f" "/tmp/fhs-code.scm")
(symlink "glibc-for-fhs-1/lib/ld-linux.so.2" "ld-linux.so.2")

;;ldconfig cache (shared libraries)
(write-fhs-code-scm "ld.so.cache")
(chdir "/etc")
(system* "guix" "build" "-r" "ld.so.cache" "-f" "/tmp/fhs-code.scm")

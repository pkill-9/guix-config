(specifications->manifest (list

;; ===============
;; ==== Desktop ==
;; ===============

"geary"
"gnome-keyring"
"mpv"
"wesnoth"
"xonotic"
"minetest"
"blender"
"steam"
"flatpak"
"zathura-pdf-poppler"

;; ===============
;; === Emacs =====
;; ===============

"emacs-paredit"
"emacs-geiser"
"emacs-yasnippet"
"emacs"
"emacs-guix"
"emacs-magit"
"emacs-evil"
"emacs-rainbow-delimiters"
"emacs-origami-el"
"emacs-lua-mode"

;; ===============
;; ==== Debug ====
;; ===============

"strace"
"gdb"
"nss-certs"
"glibc"

;; ===============
;; == Filesystem =
;; ===============

"sshfs"
"cryptsetup"
"gvfs"
"bindfs"
"unionfs-fuse"
"dosfstools"

;; ===============
;; === Fonts =====
;; ===============

"font-gnu-freefont"
"font-ghostscript"


;; ===============
;; === Hardware ==
;; ===============

"dmidecode"
"acpi"
"lshw"
"powertop"
"smartmontools"
"tlp"
"lm-sensors"
"udiskie"

;; ===============
;; === Network ===
;; ===============

"wireguard-tools"
"netcat"
"curl"
"wget"

;; =================
;; == Programming ==
;; =================

"guile"
"python2"

;; =================
;; === Shell =======
;; =================

"file"
"tree"
"st"
"xterm"
"fish"
"lf"
"ripgrep"
"wterm"
"direnv"
"fd"
"tmux"
"moreutils"
"git"
"git-lfs"
"pv"
"alacritty"
"socat"
"ncurses"
"fzf"
"foot"

;; =================
;; === System ======
;; =================

"binutils"
"htop"
"iotop"
"neovim"
"pinentry"
"jack"
"jack2"
"qjackctl"
"ghc-pandoc"
"lsof"
"imagemagick"
"password-store"
"links"
"folks"
"bubblewrap"
"gnupg"
"flatpak"
"zip"
"unzip"
"bashtop"
"glibc-locales"

;; =================
;; === Themes ======
;; =================

"breeze-icons"
"adwaita-icon-theme"

;; =================
;; === WM Utils=====
;; =================

"xbacklight"
"xev"
"unclutter"
"xclip"
"xmodmap"
"xinput"
"xrandr"
"setxkbmap"
"xhost"
"xdotool"
"xdg-utils"
"rofi"
"brightnessctl"
"slurp"
"grim"
"wl-clipboard"
"waybar"
"xset"
"mako"
"libnotify"
"swayidle"
"sway"
"swaybg"
))

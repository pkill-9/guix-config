;; ===============
;; ==== Sandbox ==
;; ===============

(use-modules
   (ice-9 pretty-print)
   (guix packages)
   (gnu packages)
   (guix utils)
   (guix git-download)
   (guix gexp)
   (gnu packages games)
   (gnu packages base)
   (gnu packages bash)
   (gnu packages guile)
   (guix build utils)
   (guix build-system trivial))

(define-public (sandboxed-package original-package)
  (package
    (name (package-name original-package))
    (version (package-version original-package))
    (source #f)
    (build-system trivial-build-system)
    (inputs (list original-package
		  guile-3.0
		  bash))
    (arguments
      `(#:modules ((guix build utils)
		   (ice-9 pretty-print))
	#:builder ,#~(begin
		     (use-modules (guix build utils) (ice-9 pretty-print))
                     (let* ((out #$output)
			    (guile #$guile-3.0)
			    (original-package #$original-package))

			   ;; Symlink all files from original package
			   (copy-recursively original-package out #: copy-file symlink)

			   ;; Replace symlinks to binaries with [guile] wrappers that call them in a sandbox, if there are any.
			   (if (file-exists? (string-append original-package "/bin"))
			       (for-each (lambda (file)

				      ;; Wrap with HOME modified
				      (delete-file file)
                                      (let ((output-port (open-file file "w")))

					;; Write shebang
					(display (string-append "#!" guile "/bin/guile --no-auto-compile" "\n!#\n\n") output-port)

					;; Write wrapper
                                        (pretty-print `(begin
							 (define REAL-HOME (getenv "HOME"))
						         (setenv "HOME" (string-append REAL-HOME ,(string-append "/.appdata/" #$(package-name original-package) "-" #$(package-version original-package))))
							 (display (string-append "\nGuix Home Sandbox: Setting $HOME to " (getenv "HOME") "\n\n") (current-error-port))
							 (apply execl (append
									(list ,(string-append original-package "/bin/" (basename file)))
							                (command-line))) ;; Need to add arguments
							)
						 output-port)
                                        (newline output-port)
                                        (close output-port))
				      (chmod file #o755)
				      )
                                    (find-files (string-append out "/bin"))))

			   ;; Patch all .desktop files to point to this package output path if there are any.
			   (if (file-exists? (string-append original-package "/share/applications"))
			       (begin
			           (delete-file-recursively (string-append out "/share/applications"))
			           (copy-recursively (string-append original-package "/share/applications")
			                	     (string-append out "/share/applications"))
			           (for-each (lambda (file)
                                          (substitute* file
			                	       ((original-package) out)))
                                        (find-files (string-append out "/share/applications")))))
			   ))))
    (home-page (package-home-page original-package))
    (synopsis (string-append (package-synopsis original-package) " (sandboxed)"))
    (description (string-append (package-description original-package) " - This is a sandbox wrapper for the package."))
    (license (package-license original-package))))

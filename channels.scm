(define (local-or-remote-url local-channel-name remote-channel-url)
  (let* ((local-channel-path
          (lambda (channel-name)
            (string-append (getenv "HOME")
                           "/.config/guix/local-channels/"
                           channel-name)))
         (local-channel-git-path
          (lambda (channel-name)
            (string-append "file://"
                           (local-channel-path
                            channel-name)))))
    (if (file-exists? (local-channel-path local-channel-name))
        (local-channel-git-path local-channel-name)
        remote-channel-url)))

(define (local-or-remote-channel channel-name remote-channel-url)
  (channel
   (name (string->symbol channel-name))
   (url
    (local-or-remote-url channel-name remote-channel-url))))

(define extra-channels
  (lambda extra-channels
    (append extra-channels
            %default-channels)))

(extra-channels
;; (channel
;;  (name 'guix)
;;  (url (string-append "file://" (getenv "HOME") "/.config/guix/local-channels/guix-jack2")))
;; (local-or-remote-channel
;;  "pkill9-free"
;;  "https://gitlab.com/pkill-9/guix-packages-free.git")
;; (local-or-remote-channel
;;  "pkill9-nonfree"
;;  "https://gitlab.com/pkill-9/guix-packages-nonfree.git")
 (local-or-remote-channel
   "nonguix"
   "https://gitlab.com/nonguix/nonguix.git")
 ;;(local-or-remote-channel ;; Failing to build
 ;; "pkill9-package-customizations"
 ;; "https://gitlab.com/pkill-9/guix-package-customizations.git")
 )

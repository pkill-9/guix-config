;; ===================================
;; ========= Utils ===================
;; ===================================

;;union currently requires options given to it, other an error "apply to non-list #f" occurs.
;;Todo: in create-inferior-package, forward build output.

(use-modules
               (guix gexp)
               (guix packages)
               (guix build-system trivial)
               (guix inferior) ;; create-inferior-package
               (guix channels) ;; create-inferior-package
               (guix utils) ;; create-inferior-package
)

(define* (union name packages #:key options)
  (computed-file name
                 (with-imported-modules `((guix build union))
                                        #~(begin
                                            (use-modules (guix build union))
                                            (union-build #$output '#$packages)))
                 #:options options))

(define (package-output->package original-package package-output)
  (package
   (name (string-append (package-name original-package) "-" package-output))
   (version (package-version original-package))
   (source #f)
   (build-system trivial-build-system)
   (inputs
    `(("package" ,original-package ,package-output)))
   (arguments
    `(#:modules ((guix build utils))
      #:builder
      (begin
        (use-modules (guix build utils))
        (let* ((out (assoc-ref %outputs "out"))
               (original-package (assoc-ref %build-inputs "package")))
          (symlink original-package out)))))
   (home-page #f)
   (synopsis (string-append "Output " package-output
                            " of package: " (package-name original-package)))
   (description synopsis)
   (license (package-license original-package))))

(define* (executable-mixed-text-file name #:rest text)
  "Return an object representing store file NAME containing TEXT.  TEXT is a
sequence of strings and file-like objects, as in:

  (mixed-text-file \"profile\"
                   \"export PATH=\" coreutils \"/bin:\" grep \"/bin\")

This is the declarative counterpart of 'text-file*'. The resulting file is made executable."
  (define build
    (gexp (call-with-output-file (ungexp output "out")
            (lambda (port)
              (display (string-append (ungexp-splicing text)) port)
              (chmod (ungexp output "out") #o755)))))

  (computed-file name build))


(define* (executable-plain-file name text)
  "Return an object representing store file NAME containing TEXT.  TEXT is a
a string.

This is equivalent to plain-file, but the file is executable."
  (define build
    (gexp (call-with-output-file (ungexp output "out")
            (lambda (port)
              (display (ungexp text) port)
              (chmod (ungexp output "out") #o755)))))

  (computed-file name build))

(define* (create-bash-script script-name script-text paths-to-executables #:optional paths-to-arbitrary-files)
  ;;Create a bash script with executable permission
  ;;
  ;;all commands will have variables with their names pointing to the store path, so the bash script must use for example $mkdir instead of mkdir.
  ;;
  ;;paths-to-executables consists of a list of lists, which are composed of (<package-object> "/subdirectory/" "executable")
  ;; for example, (list (list coreutils "/bin/" "mkdir") (list grep "/bin/ "grep"))
  ;; However, if only two list items exist, then assume that the first is a store path direct to the executable, and the second is the variable name
  ;;
  ;; Paths-to-other-files consists of a list of lists, which are composed of (<package-object> "/path/to/file" "variable-name")
  ;; Maybe instead make a function that takes these, idk
  (let* ((executables-paths-header-lines
          (map
           (lambda (path)
             (if (> (length path) 2)
                 ;;If more than two list items
                 (list
                  (list-ref path 2) ;; Variable name
                  "="
                  (list-ref path 0) ;; Package object (executable-mixed-text-file converts to store path)
                  (list-ref path 1) ;; Subdirectory
                  (list-ref path 2) ;; Executable
                  "\n")
                 ;;Else
                 (list
                  (list-ref path 1) ;; Variable name
                  "="
                  (list-ref path 0) ;; Executable
                  "\n")))
           paths-to-executables))
         (arbitrary-paths-header-lines
          (if paths-to-arbitrary-files
              (map
               (lambda (path)
                 (list
                  (list-ref path 2) ;;Variable name
                  "="
                  (list-ref path 0) ;; Package object (executable-mixed-text-file converts to store path)
                  (list-ref path 1) ;; Subpath to file
                  "\n"
                  ))
               paths-to-arbitrary-files)
              (list)))
         (smooshed-header-lines (append (apply append executables-paths-header-lines)
                                        (apply append arbitrary-paths-header-lines)))
         )
    (apply executable-mixed-text-file (append (list script-name)
                                              (list "#!" (@ (gnu packages bash) bash) "/bin/bash" "\n\n")
                                              smooshed-header-lines
                                              (list "\n\n")
                                              (list script-text)))
    ))

(define* (create-inferior-package #:key
                                  guix-commit
                                  package-code-to-evaluate)
  (let* ((inferior-package (@@ (guix inferior) inferior-package)) ;; Localise private function
         (%inferior (inferior-for-channels
                     (list (channel
                            (name 'guix)
                            (url "https://git.savannah.gnu.org/git/guix.git")
                            (commit guix-commit)))))
         (pkg-info
          (inferior-eval
           `(begin
              (let* ((new-package ,package-code-to-evaluate)
                     (new-package-id (object-address new-package)))
                (hashv-set! %package-table new-package-id new-package) ;; Add the new package's object-address (assigned to id) to the inferior's %package-table
                `(,(package-name new-package)
                  ,(package-version new-package)
                  ,new-package-id)));; Return the package info for use with (inferior-package)
           %inferior)))
    (inferior-package %inferior
                      (list-ref pkg-info 0)
                      (list-ref pkg-info 1)
                      (list-ref pkg-info 2)))) ;; Return the inferior-package

;; ===================================
;; ========= Custom services =========
;; ===================================

;; Maybe use separate caches for different architectures of glibc, so that binaries don't look for incorrect architectures? Then again I don't think it's a problem.
;; Move generation of package union to outside packages->ld.so.conf and make that function able to generate the conf file using any list of packages/store-paths.
;; Use configuration record for service that includes glibc files, additional packages to put in system profile (for paths and share data), and arbitrary additional paths, all with sane defaults.
;; WARNING: special-files-service doesn't raise an error ("" ,(@ (gnu packages ) ))f given invalid arguemnt, e.g. a "#f" or an empty list - the operating system will error on boot and drop into a guile shell when it fails to be able to use that argument to place special files.
;; Electron apps are failing due to missing fonts, and fail catastrophically and dont even give an error message other than "breakpoint trap"
(use-modules
             (ice-9 ftw) ;; for creating recursive list of directories of libs for FHS  #:use-module (guix download)
             (srfi srfi-1) ;; For "remove" for delete-service
             (guix records) ;; For defining record types
             (guix profiles) ;; for specifications->manifest and manifest-entries
             (gnu services)
             (gnu services shepherd)
             (guix gexp)
             (guix build-system trivial)
             (guix packages)
             (gnu system accounts) ;; For 'user-account'
             (gnu system shadow) ;; For 'account-service-type'
             (gnu packages)
             (gnu packages linux)
             (gnu packages bash)
             (gnu packages base)
)

(define-public (delete-service service-type services)
  (remove
   (lambda (service)
     (eq? (service-kind service) service-type))
   services))

  (define-public %brightness-service
    ;; Run as a Shepherd service instead of an activation script since the
    ;; latter typically runs before all modules have been loaded.
    (simple-service 'brightness-service shepherd-root-service-type
                    (list (shepherd-service
                           (provision '(bash))
                           (requirement '())
                           (start #~(lambda ()
                                      (invoke
                                       #$(file-append bash "/bin/bash")
                                       "-c"
                                       "echo 50 > /sys/class/backlight/intel_backlight/brightness")))
                           (respawn? #f)))))

  (define-public %powertop-service
    ;; Run as a Shepherd service instead of an activation script since the
    ;; latter typically runs before all modules have been loaded.
    (simple-service 'powertop shepherd-root-service-type
                    (list (shepherd-service
                           (provision '(powertop))
                           (requirement '())
                           (start #~(lambda ()
                                      (invoke
                                       #$(file-append powertop "/sbin/powertop")
                                       "--auto-tune")))
                           (respawn? #f)))))

;;==================================
;;========== Nonfree Linux =========
;;==================================

(use-modules ((guix licenses) #:prefix license:)
               (gnu packages)
               (gnu packages linux)
               (gnu packages tls)
               (guix build-system trivial)
               (guix git-download)
               (guix packages)
               (guix download)
               (guix utils) ;; version-major
)

(define %boot-logo-patch
  ;; Linux-Libre boot logo featuring Freedo and a gnu.
  (origin
   (method url-fetch)
   (uri (string-append "http://www.fsfla.org/svn/fsfla/software/linux-libre/"
                       "lemote/gnewsense/branches/3.16/100gnu+freedo.patch"))
   (sha256
    (base32
     "1hk9swxxc80bmn2zd2qr5ccrjrk28xkypwhl4z0qx4hbivj7qm06"))))

(define* (create-linux-package-data #:key
                                    version source-checksum
                                    (name "linux"))
  (let* ((major-version (version-major version))
         (source-url (string-append
                      "https://"
                      "www.kernel.org"
                      "/pub/linux/kernel"
                      "/v" major-version ".x/"
                      "linux-" version ".tar.xz")))
    `(package (inherit
               (@ (gnu packages linux) linux-libre))
              (name ,name)
              (version ,version)
              (source (origin
                       (method url-fetch)
                       (uri ,source-url)
                       (sha256
                        (base32
                         ,source-checksum))))
              (synopsis "Mainline Linux kernel, nonfree binary blobs included.")
              (description "Linux is a kernel.")
              ;;(license license:gpl2) ;;needs an import of license
              (home-page "http://kernel.org/"))))

(define* (create-linux-package-inferior #:key
                                        version
                                        source-checksum
                                        guix-commit)
  (create-inferior-package #:guix-commit guix-commit
                           #:package-code-to-evaluate
                           (create-linux-package-data #:version version
                                                      #:source-checksum source-checksum)))
;;; Forgive me Stallman for I have sinned.

(define-public radeon-firmware-non-free
  (package
   (name "radeon-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/radeon/")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source
                                          (lambda (file stat)
                                            (string-contains file "radeon"))))
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for Radeon integrated chips")
   (description "Non-free firmware for Radeon integrated chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public ath10k-firmware-non-free
  (package
   (name "ath10k-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/")))
                    (mkdir-p fw-dir)
                    (copy-recursively (string-append source "/ath10k")
                                      (string-append fw-dir "/ath10k"))
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for ath10k wireless chips")
   (description "Non-free firmware for ath10k integrated chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public linux-firmware-non-free
  (package
   (name "linux-firmware-non-free")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/")))
                    (mkdir-p fw-dir)
                    (copy-recursively source fw-dir)
                    #t))))

   (home-page "")
   (synopsis "Non-free firmware for Linux")
   (description "Non-free firmware for Linux")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.radeon_firmware;hb=HEAD"))))

(define-public iwlwifi-firmware-nonfree
  (package
   (name "iwlwifi-firmware-nonfree")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source "iwlwifi-.*\\.ucode$|LICENCE\\.iwlwifi_firmware$"))
                    #t))))

   (home-page "https://wireless.wiki.kernel.org/en/users/drivers/iwlwifi")
   (synopsis "Non-free firmware for Intel wifi chips")
   (description "Non-free firmware for Intel wifi chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.iwlwifi_firmware;hb=HEAD"))))

(define-public ibt-hw-firmware-nonfree
  (package
   (name "ibt-hw-firmware-nonfree")
   (version "65b1c68c63f974d72610db38dfae49861117cae2")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git")
                  (commit version)))
            (sha256
             (base32
              "1anr7fblxfcrfrrgq98kzy64yrwygc2wdgi47skdmjxhi3wbrvxz"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
                  (use-modules (guix build utils))
                  (let ((source (assoc-ref %build-inputs "source"))
                        (fw-dir (string-append %output "/lib/firmware/intel")))
                    (mkdir-p fw-dir)
                    (for-each (lambda (file)
                                (copy-file file
                                           (string-append fw-dir "/"
                                                          (basename file))))
                              (find-files source "ibt-hw-.*\\.bseq$|LICENCE\\.ibt_firmware$"))
                    #t))))

   (home-page "http://www.intel.com/support/wireless/wlan/sb/CS-016675.htm")
   (synopsis "Non-free firmware for Intel bluetooth chips")
   (description "Non-free firmware for Intel bluetooth chips")
   ;; FIXME: What license?
   (license (license:non-copyleft "http://git.kernel.org/?p=linux/kernel/git/firmware/linux-firmware.git;a=blob_plain;f=LICENCE.ibt_firmware;hb=HEAD"))))

;; ==========================
;; ======== Firejail ========
;; ==========================


(use-modules
               (gnu packages)
               (guix packages)
               (guix git-download)
               (guix build-system gnu)
               (guix licenses))


(define-public firejail
(package
  (name "firejail")
  (version "0.9.64.4")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
            (url "https://github.com/netblue30/firejail")
            (commit version)))
      (sha256
        (base32
          "0fqbam25gh62bpwmda2l6kb9ln818jdi5m76fqbma7p7rkqwpxdb"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f
     ;;#:make-flags '("CC=gcc" "RM=rm" "SHELL=sh" "all")
     ;#:phases (modify-phases %standard-phases (delete 'configure))
     ))
  (home-page "https://firejail.wordpress.com")
  (synopsis "Sandboxing application")
  (description "Firejail is a SUID program that reduces the risk of security breaches by restricting the running environment of untrusted applications using Linux namespaces and seccomp-bpf.  It allows a process and all its descendants to have their own private view of the globally shared kernel resources, such as the network stack, process table, mount table.")
  (license gpl3+)))

;; ==========================
;; ========= Config =========
;; ==========================

(use-modules
  (gnu) ;; use-service-modules and probably use-package-modules.
  (gnu services) ;; "(service)"
  (gnu system) ;; Provides things like operating-system-services etc
  (gnu system shadow) ;; account-service-type
  (gnu system accounts) ;; user-account
  (gnu packages) ;; specifications->manifest
  (guix gexp)
  ;;(pkill9 packages anbox) ;; anbox linux kernel modules. - disabled cos it's disabled in the repository as bzr-download module import is causing issues (i think the linux-inferior just needs to use an updated guix which has bzr-download.). anbox doesn't work yet anyway due to the linux container not working.
  (gnu system) ;; %setuid-programs
  (guix store)) ;; %default-substitute-urls.

(use-service-modules xorg ;; xorg-configuration
                     shepherd ;; shepherd-root-service-type
                     admin ;; rottlog
                     avahi ;; avahi-service-type
                     dbus ;; polkit-service-type, dbus stuff
                     nix ;; nix-service-type
                     mcron ;; mcron
                     linux ;; earlyoom-service-type
                     pm ;; tlp-service-type
                     desktop ;; gnome-desktop-service
                     ssh ;; openssh-service-type
                     cups ;; cups-service-type
                     networking ;; tor
                     vpn ;; openvpn
                     virtualization ;; libvirt-service-type and virtlog-service-type, and qemu-binfmt-service-type
                     sound ;; pulseaudio-service-type
                     sddm) ;; sddm-service-type

(use-package-modules certs ;; nss-certs
                     bash ;; bash
                     libusb ;; libmtp
                     nfs ;; nfs-utils
                     cups ;; hplip-minimal
                     gnome ;; gnome-shell-extensions
                     wm ;; sway
                     linux) ;; bluez

(define-public base-system-config
  (operating-system
   
   (host-name "antelope")

   (timezone "Europe/London")

   (locale "en_GB.utf8")

   (keyboard-layout (keyboard-layout "us"
                                     #:options '("ctrl:nocaps"))) ;; change caps to ctrl

   (bootloader
    (bootloader-configuration
     (bootloader grub-efi-bootloader)
     (target "/boot/efi")
     (keyboard-layout keyboard-layout)))

   (mapped-devices
    (list
     (mapped-device
      (source
       (uuid "33ad4f47-a837-4c69-855e-3f8e2e9f00c0"))
      (target "home-partition")
      (type luks-device-mapping)))) ;; Encrypted
   
   (file-systems
    (cons*
     (file-system
      (device
       (file-system-label "my-root"))
      (mount-point "/")
      (type "ext4"))
     (file-system
      (device "/dev/mapper/home-partition") ;; Encrypted home partition
      (mount-point "/home")
      (type "ext4")
      (dependencies mapped-devices))
     %base-file-systems))
   
   (swap-devices
    '("/mnt/swapfile")) ;; Swap partition is used for intel rapid start.
   
   (users
    (cons*
     (user-account
      (name "itsme")
      (group "users")
      (supplementary-groups
       '("wheel" ;; Sudo priviledges
         "netdev")) ;; Not sure what this is for
      (home-directory "/home/itsme"))
     (user-account
      (name "personal")
      (group "users")
      (supplementary-groups
       '("wheel" ;; Sudo priviledges
         "netdev")) ;; Not sure what this is for
      (home-directory "/home/personal"))
     %base-user-accounts))

   (name-service-switch
    %mdns-host-lookup-nss))) ;; Allow resolution of '.local' host names with mDNS.

(define xorg-intel-screen-tearing-fix
  "Section \"Device\"
      Identifier  \"Intel Graphics\"
      Driver      \"intel\"
      Option      \"AccelMethod\"  \"sna\"
   Option         \"TearFree\" \"true\"
   EndSection")

(define xorg-enable-dri3
  "Section \"Device\"
      Identifier  \"Intel Graphics\"
      Driver      \"intel\"
      Option      \"DRI\"  \"3\"
   EndSection")

(define xorg-trackpoint-setup
  "Section \"InputClass\"
     Identifier \"Clickpad\"
     MatchIsTouchpad \"on\"
     Driver \"libinput\"
     Option  \"ClickMethod\"   \"clickfinger\"
   EndSection")

(define xorg-touchpad-setup
  "Section \"InputClass\"
     Identifier \"Clickpad\"
     MatchIsTouchpad \"on\"
     Driver \"libinput\"
     Option  \"DisableWhileTyping\"   \"0\"
   EndSection")

(define %udev-rule-steam-controller-permissions
  ;; https://wiki.gentoo.org/wiki/Steam_Controller
  ;; https://steamcommunity.com/app/353370/discussions/2/1735465524711324558/
  (file->udev-rule
   "90-steam-controller-permissions.rules"
   (mixed-text-file "99-steam-controller-permissions.rules"
                    "# This rule is needed for basic functionality of the controller in Steam and keyboard/mouse emulation\n"
                    "SUBSYSTEM==\"usb\", ATTRS{idVendor}==\"28de\", MODE=\"0666\"\n"
                    "# This rule is necessary for gamepad emulation; make sure you replace 'pgriffais' with a group that the user that runs Steam belongs to\n"
                    "KERNEL==\"uinput\", MODE=\"0660\", GROUP=\"users\", OPTIONS+=\"static_node=uinput\"\n"
                    "# Valve HID devices over USB hidraw\n"
                    "KERNEL==\"hidraw*\", ATTRS{idVendor}==\"28de\", MODE=\"0666\"\n"
                    "# Valve HID devices over bluetooth hidraw\n"
                    "KERNEL==\"hidraw*\", KERNELS==\"*28DE:*\", MODE=\"0666\"\n")))

(define %udev-rule-backlight-permissions
  (file->udev-rule
   "90-backlight-brightness-permissions.rules"
   (mixed-text-file "90-backlight-brightness-permissions.rules"
                    "ACTION==\"add\", "
                    "SUBSYSTEM==\"backlight\", "
                    "KERNEL==\"intel_backlight\", "
                    "RUN+=\"" (file-append
                               (canonical-package
                                (@ (gnu packages base)
                                   coreutils))
                               "/bin/chgrp") " video /sys/class/backlight/%k/brightness\""
                    "\n"
                    "ACTION==\"add\", "
                    "SUBSYSTEM==\"backlight\", "
                    "KERNEL==\"intel_backlight\", "
                    "RUN+=\"" (file-append
                               (canonical-package
                                (@ (gnu packages base)
                                   coreutils))
                               "/bin/chmod") " g+w /sys/class/backlight/%k/brightness\"")))

(define %garbage-collector-job-itsme
  ;; Collect garbage 5 minutes after midnight every day.
  ;; The job's action is a shell command.
  #~(job "5 0 * * *"            ;Vixie cron syntax
         "guix gc --delete-generations=1w"
	 #:user "itsme"))

(define %garbage-collector-job-system
  ;; Collect garbage 5 minutes after midnight every day.
  ;; The job's action is a shell command.
  #~(job "5 0 * * *"            ;Vixie cron syntax
         "guix system delete-generations 1w"))

(define-public desktop-config
  (operating-system
   (inherit base-system-config)

   (kernel-arguments
    ;; https://bugzilla.kernel.org/show_bug.cgi?id=199057
    ;; http://www.thinkwiki.org/wiki/Installing_Gentoo_on_an_X240
    ;;      #Brightness_Controls_.28F5_.26_F6.29
    ;; Fix brightness keys:
    ;;   https://wiki.archlinux.org/index.php/
    ;;           Lenovo_ThinkPad_X220#Backlight
    '("mem_sleep_default=deep"
      ;; https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1722478/comments/69
      ;; https://bugzilla.redhat.com/show_bug.cgi?id=1442699
      "psmouse.synaptics_intertouch=0"
      ;; Disable USB autosuspend: https://unix.stackexchange.com/questions/91027/how-to-disable-usb-autosuspend-on-kernel-3-7-10-or-above
      "usbcore.autosuspend=-1"
      "iwlwifi.bt_coex_active=0" ;; Fix bluetooth cutting out: https://wiki.archlinux.org/index.php/Bluetooth#Intel_combined_wifi_and_bluetooth_cards
      "quiet" ;; Prevent kernel cluttering up the console output with unimportant messages
      ))

   (users
    (cons*
     (user-account
      (name "itsme")
      (group "users")
      (supplementary-groups
       ;; Shouldn't add users to audio group when running Pulseaudio:
       ;;   https://www.freedesktop.org/wiki/Software/PulseAudio/
       ;;           Documentation/User/PerfectSetup/
       '("wheel" ;; sudo privileges
         "netdev" ;; not sure
         "video" ;; GPU acceleration I think, or webcam.
         "kvm" "libvirt" ;; virtualisation with qemu
         "lp" ;; printing?
         ))
      (home-directory "/home/itsme"))
     (user-account
      (name "test")
      (group "users")
      (supplementary-groups
       '("video"))
      (home-directory "/home/test"))
     %base-user-accounts))

   (setuid-programs
    (cons
     #~(string-append #$firejail "/bin/firejail")
     %setuid-programs))

   (packages
    (cons*
     nss-certs ;; Weechat doesn't find server certificates without nss-certs.
     network-manager
     bluez ;; bluetoothctl
     %base-packages))

   (services
    (list
     ;;system management
;;     (simple-service 'my-cron-jobs
;;                     mcron-service-type
;;                     (list %garbage-collector-job-itsme
;;			   %garbage-collector-job-system))
     (service thermald-service-type)
     (service earlyoom-service-type)
     (service tlp-service-type) ;; supposed to increase battery life I think
     (service nix-service-type)
     %powertop-service
     ;;desktop
;;     (service sddm-service-type)
;;     (service gnome-desktop-service-type)
;;     (service xfce-desktop-service-type)
     (simple-service 'user-modifiable-backlight-permissions
       udev-service-type
       (list %udev-rule-backlight-permissions))
     (screen-locker-service swaylock "swaylock")
     (screen-locker-service kbd "vlock")
;;     (service fhs-binaries-compatibility-service-type
;;              (fhs-configuration
;;               ;;(lib-packages fhs-packages) ;; Add fs libraries manually, to avoid mass downloads/builds on system reconfigure.
;;               (lib-packages '())
;;               (additional-special-files
;;                `(("/usr/share/X11/xkb" ;; QT apps fail to recieve keyboard input unless they find this hardcoded path.
;;                   ,(file-append
;;                     (canonical-package
;;                      (@ (gnu packages xorg) xkeyboard-config))
;;                     "/share/X11/xkb"))
;;                  ("/etc/fonts" ,"/run/current-system/profile/etc/fonts"))))) ;; Chromium component of electron apps break without fontconfig configuration here.
     (pam-limits-service ;; JACK fails to run without these PAM limits set. - but now it's not fixing the issue...
      (list (pam-limits-entry "@users" 'both 'rtprio 99)
            (pam-limits-entry "@users" 'both 'memlock 'unlimited)))
     (service libvirt-service-type ;; For virtualisation using Qemu.
              (libvirt-configuration
               (unix-sock-group "libvirt")))
     (service virtlog-service-type) ;; Something to do with Qemu.
     ;;Compiling to other systems
     (service qemu-binfmt-service-type
              (qemu-binfmt-configuration
               (platforms (lookup-qemu-platforms "arm"))
               ;;(guix-support? #t)
	       ))
     ;;connectivity
     (bluetooth-service #:auto-enable? #t) ;; enable bluetooth support - use bluetoothctl (in bluez package) to handle bluetooth devices
     (service cups-service-type ;; For printing.
              (cups-configuration
  	       (web-interface? #t)
  	       (extensions
  	        (list cups-filters
                      hplip-minimal)))) ;; HP printer support
     (service openssh-service-type)
     ;; Copy of %desktop-services, but modified
              ;; Screen lockers are a pretty useful thing and these are small.
         ;;(screen-locker-service slock)
         ;;(screen-locker-service xlockmore "xlock")

         ;; Add udev rules for MTP devices so that non-root users can access
         ;; them.
         (simple-service 'mtp udev-service-type (list libmtp))
         ;;Add udev rules to enable configuration of Steam controller
         (simple-service 'steam-controller-configuration udev-service-type (list %udev-rule-steam-controller-permissions))
         ;; Add udev rules for scanners.
         (service sane-service-type)
         ;; Add polkit rules, so that non-root users in the wheel group can
         ;; perform administrative tasks (similar to "sudo").
         polkit-wheel-service

         ;; Allow desktop users to also mount NTFS and NFS file systems
         ;; without root.
;;         (simple-service 'mount-setuid-helpers setuid-program-service-type
;;                         (list (file-append nfs-utils "/sbin/mount.nfs")
;;                               (file-append ntfs-3g "/sbin/mount.ntfs-3g")))

         ;; The global fontconfig cache directory can sometimes contain
         ;; stale entries, possibly referencing fonts that have been GC'd,
         ;; so mount it read-only.
         fontconfig-file-system-service

         ;; NetworkManager and its applet.
         (service network-manager-service-type)
         (service wpa-supplicant-service-type)    ;needed by NetworkManager
         ;;(simple-service 'network-manager-applet
         ;;                profile-service-type
         ;;                (list network-manager-applet))
         (service modem-manager-service-type)
         (service usb-modeswitch-service-type)

         ;; The D-Bus clique.
         (service avahi-service-type)
         (udisks-service)
         (service upower-service-type)
         (accountsservice-service)
         (service cups-pk-helper-service-type)
         (service colord-service-type)
         (geoclue-service)
         (service polkit-service-type)
         (service elogind-service-type
			(elogind-configuration
			  (handle-lid-switch-external-power 'ignore)))
         (dbus-service)

         (service ntp-service-type)

         x11-socket-directory-service

         ;;(service pulseaudio-service-type)
         (service alsa-service-type)

         (service login-service-type)

         (service virtual-terminal-service-type)
         (service console-font-service-type
                  (map (lambda (tty)
                         (cons tty %default-console-font))
                       '("tty2" "tty3" "tty4" "tty5" "tty6")))

         (service agetty-service-type (agetty-configuration
                                       (extra-options '("-L")) ; no carrier detect
                                       (term "vt100")
                                       (tty #f))) ; automatic

         ;;(service mingetty-service-type (mingetty-configuration
         ;;                                (tty "tty1")))
         (service mingetty-service-type (mingetty-configuration
                                         (tty "tty2")))
         (service mingetty-service-type (mingetty-configuration
                                         (tty "tty3")))
         (service mingetty-service-type (mingetty-configuration
                                         (tty "tty4")))
         (service mingetty-service-type (mingetty-configuration
                                         (tty "tty5")))
         (service mingetty-service-type (mingetty-configuration
                                         (tty "tty6")))
         (simple-service 'switch-to-tty2 shepherd-root-service-type
                         (list (shepherd-service
                                (provision '(kdb))
                                (requirement '(virtual-terminal))
                                (start #~(lambda ()
                                           (invoke #$(file-append kbd "/bin/chvt") "2")))
                                (respawn? #f))))

         ;;(static-networking-service "lo" "127.0.0.1/24")
	 (service static-networking-service-type (list %loopback-static-networking))
         (syslog-service)
         (service urandom-seed-service-type)
         (service guix-service-type
                (guix-configuration
                  (substitute-urls
                   (list
                    "https://bordeaux.guix.gnu.org"
                    "https://ci.guix.gnu.org"
                    "https://berlin.guixsd.org"))
                  (extra-options
                   '("--cores=4"))))
         (service nscd-service-type)

         (service rottlog-service-type)

         ;; The LVM2 rules are needed as soon as LVM2 or the device-mapper is
         ;; used, so enable them by default.  The FUSE and ALSA rules are
         ;; less critical, but handy.
         (service udev-service-type
                  (udev-configuration
                   (rules (list lvm2 fuse alsa-utils crda))))

         (service special-files-service-type
                  `(("/bin/sh" ,(file-append bash "/bin/sh"))
                    ("/usr/bin/env" ,(file-append coreutils "/bin/env"))
                    ("/etc/cups/cupsd.conf" ,(file-append cups "/etc/cups/cupsd.conf")))))) ;; Fix to load CUPS admin page. https://issues.guix.gnu.org/39801

   (name-service-switch 
    %mdns-host-lookup-nss))) ;; Allow resolution of '.local' host names with mDNS.

(define linux-5.7.7
  (create-linux-package-inferior #:version "5.7.7"
                                 #:source-checksum
                                 "0zzff78fjn0a87lr1j11hx97gcpbf0q5qn0nalb3qd43j9kvjh7q"
                                 #:guix-commit
                                 "fe743ebb41f0936b378a185beb7c00adc4172d8c"))

(define linux-5.10.24
  (create-linux-package-inferior #:version "5.10.24"
                                 #:source-checksum
                                 "0gvnplip90gvlzw9rm0cg66z54cfa82gk23icf5xdickb17d1p66"
                                 #:guix-commit
                                 "2892e1a2a8bbe7d43854f40dc759ad3e0faecfbc"))

(define linux-5.19
  (create-linux-package-inferior #:version "5.19"
                                 #:source-checksum
                                 "1a05a3hw4w3k530mxhns96xw7hag743xw5w967yazqcykdbhq97z"
                                 #:guix-commit
                                 "c4e58c119d637ef434c4b52437f677dfb69eb091"))

;;(define linux-5.5.9
;;  (create-linux-package-inferior #:version "5.5.9"
;;                                 #:source-checksum
;;                                 "0y58gkzadjwfqfry5568g4w4p2mpx2sw50sk95i07s5va1ly2dd4"
;;                                 #:guix-commit
;;                                 "72e76113f7731190881267856d3f91775450f147"))

;;(define linux-5.4.6
;;  (create-linux-package-inferior #:version "5.4.6"
;;                                 #:source-checksum
;;                                 "1j4916izy2nrzq7g6m5m365r60hhhx9rqcanjvaxv5x3vsy639gx"
;;                                 #:guix-commit
;;                                 "92fcf9856face3822ce827d7732c4b152f715f62"))

(define-public proprietary-desktop-config
  (operating-system
   (inherit desktop-config)
   (kernel linux-5.19)
   (firmware
    (cons*
     iwlwifi-firmware-nonfree ;; wifi
     ibt-hw-firmware-nonfree ;; bluetooth
     %base-firmware)) ;; would use operating-system-firmware but it is missing an export in gnu/system.
    ))


;;base-system-config
;;desktop-config
proprietary-desktop-config
;;linux-5.19
